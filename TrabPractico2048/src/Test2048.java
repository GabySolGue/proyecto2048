import static org.junit.jupiter.api.Assertions.*;

import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import logica.Juego;
import logica.Tablero;

class Test2048 {

	@BeforeEach
	void setUp() throws Exception { }

	@Test
	public void juegoDesHabilitadoTest() 
	{
		Juego juego = new Juego();
		juego.nuevaPartida("Test");
		juego.tablero().get()[0][0] = 2048;
		juego.actualizarPuntaje();
		juego.juegoTerminado();
		
		assertFalse(juego.getHabilitado());
	}
	
	@Test
	public void tableroLlenoConJugadasPosiblesTest() 
	{
		//matriz:			
//	     2 2 4 2			
//	     8 8 8 8			
//	     4 4 2 2			
//	     16 16 4 4			
		Juego juego = new Juego(); //Creo la grilla inicial
		juego.nuevaPartida("Test");
		juego.tablero().get()[0][0] = 2;						
		juego.tablero().get()[0][1] = 2;						
		juego.tablero().get()[0][2] = 4;
		juego.tablero().get()[0][3] = 2;		
		juego.tablero().get()[1][0] = 8;						
		juego.tablero().get()[1][1] = 8;
		juego.tablero().get()[1][2] = 8;
		juego.tablero().get()[1][3] = 8;		
		juego.tablero().get()[2][0] = 4;						
		juego.tablero().get()[2][1] = 4;
		juego.tablero().get()[2][2] = 2;
		juego.tablero().get()[2][3] = 2;						
		juego.tablero().get()[3][0] = 16;						
		juego.tablero().get()[3][1] = 16;
		juego.tablero().get()[3][2] = 4;
		juego.tablero().get()[3][3] = 4;
		
		Juego juegoAbajo = new Juego(); //Como deberia quedar tras mover (=)
		juegoAbajo.tablero().get()[0][0] = 2;						
		juegoAbajo.tablero().get()[0][1] = 2;						
		juegoAbajo.tablero().get()[0][2] = 4;
		juegoAbajo.tablero().get()[0][3] = 2;		
		juegoAbajo.tablero().get()[1][0] = 8;						
		juegoAbajo.tablero().get()[1][1] = 8;
		juegoAbajo.tablero().get()[1][2] = 8;
		juegoAbajo.tablero().get()[1][3] = 8;		
		juegoAbajo.tablero().get()[2][0] = 4;						
		juegoAbajo.tablero().get()[2][1] = 4;
		juegoAbajo.tablero().get()[2][2] = 2;
		juegoAbajo.tablero().get()[2][3] = 2;						
		juegoAbajo.tablero().get()[3][0] = 16;						
		juegoAbajo.tablero().get()[3][1] = 16;
		juegoAbajo.tablero().get()[3][2] = 4;
		juegoAbajo.tablero().get()[3][3] = 4;
		
		juego.mover(40); //simulo movimiento abajo
		
		assertFalse(juego.juegoTerminado());
		
		juego.mover(40);
		assertEquals(juegoAbajo,juego);		
	}
	
	@Test
	public void sugerenciaJugadaContiguosTest() 
	{
//		matriz1:		
//	    2 2 4 -		    	
//	    - - - -		 		
//		- 8 16 8		 			
//		- 8 16 2				
		Juego j = new Juego(); //Creo la grilla inicial
		j.tablero().get()[0][0] = 2;						
		j.tablero().get()[0][1] = 2;						
		j.tablero().get()[0][2] = 4;
		j.tablero().get()[2][1] = 8;						
		j.tablero().get()[2][2] = 16;
		j.tablero().get()[2][3] = 8;
		j.tablero().get()[3][1] = 8;						
		j.tablero().get()[3][2] = 16;						
		j.tablero().get()[3][3] = 2;				

		//j.actualizarPuntaje();
		
		int[] esperado = {2,2,40};
		
		int[] resultado = j.sugerenciaJugada(); //simulo movimiento a la derecha

		assertArrayEquals(esperado, resultado);
	}
	
	@Test
	public void sugerenciaJugadaDistanciadosTest() 
	{
//		matriz1:		
//	    2 2 4 -		    	
//	    32 - - 32		 		
//		- 8 - -		 			
//		- 8 16 2				
		Juego j2 = new Juego(); //Creo la grilla inicial
		j2.tablero().get()[0][0] = 2;						
		j2.tablero().get()[0][1] = 2;						
		j2.tablero().get()[0][2] = 4;
		j2.tablero().get()[1][0] = 32;						
		j2.tablero().get()[1][3] = 32;
		j2.tablero().get()[2][1] = 8;
		j2.tablero().get()[3][1] = 8;
		j2.tablero().get()[3][2] = 16;						
		j2.tablero().get()[3][3] = 2;				
		
		int[] esperado = {1,0,39};
		
		int[] resultado = j2.sugerenciaJugada(); //simulo movimiento a la derecha

		assertArrayEquals(esperado, resultado);
	}
	
	@Test
	public void testDerecha4Filas4Casos() 
	{
//		matriz1:		matrizResultDer:
//	    2 2 4 -		    	- - 4 4
//	    2 2 4 4		 		- - 4 8
//		4 2 2 -	 			- - 4 4	
//		2 2 2 4				- 2 4 4
		Juego juego = new Juego(); //Creo la grilla inicial
		juego.tablero().get()[0][0] = 2;						
		juego.tablero().get()[0][1] = 2;						
		juego.tablero().get()[0][2] = 4;	
		juego.tablero().get()[1][0] = 2;					
		juego.tablero().get()[1][1] = 2;					
		juego.tablero().get()[1][2] = 4;			     	
		juego.tablero().get()[1][3] = 4;	
		juego.tablero().get()[2][0] = 4;						
		juego.tablero().get()[2][1] = 2;						
		juego.tablero().get()[2][2] = 2;	
		juego.tablero().get()[3][0] = 2;						
		juego.tablero().get()[3][1] = 2;						
		juego.tablero().get()[3][2] = 2;			     		
		juego.tablero().get()[3][3] = 4;
		
		Juego juegoAlmoverDerecha4 = new Juego(); //Creo la grilla resultante esperada
		juegoAlmoverDerecha4.tablero().get()[0][2] = 4;					     
		juegoAlmoverDerecha4.tablero().get()[0][3] = 4;	
		juegoAlmoverDerecha4.tablero().get()[1][2] = 4;					    
		juegoAlmoverDerecha4.tablero().get()[1][3] = 8;	
		juegoAlmoverDerecha4.tablero().get()[2][2] = 4;					            
		juegoAlmoverDerecha4.tablero().get()[2][3] = 4;	
		juegoAlmoverDerecha4.tablero().get()[3][1] = 2;					     	     
		juegoAlmoverDerecha4.tablero().get()[3][2] = 4;					           
		juegoAlmoverDerecha4.tablero().get()[3][3] = 4;	
															   	
		juego.mover(39); //simulo movimiento a la derecha
		
		assertEquals(juegoAlmoverDerecha4,juego);
	}
	
	@Test
	public void testDerechaDobleSuma() 
	{
		//matriz:		matrizResultDer:
//	     2 2 2 4		- 2 4 4
//	     - - - -		- - - -
//	     2 2 2 4		- 2 4 4
//	     - - - -		- - - -
		Juego juego = new Juego(); //Creo la grilla inicial
		juego.tablero().get()[0][0] = 2;						
		juego.tablero().get()[0][1] = 2;						
		juego.tablero().get()[0][2] = 2;			     		
		juego.tablero().get()[0][3] = 4;
		juego.tablero().get()[2][0] = 2;						
		juego.tablero().get()[2][1] = 2;						
		juego.tablero().get()[2][2] = 2;			     		
		juego.tablero().get()[2][3] = 4;
				
		Juego juegoAlMoverDerecha = new Juego(); //Creo la grilla resultante esperada
		juegoAlMoverDerecha.tablero().get()[0][1] = 2;					     	     
		juegoAlMoverDerecha.tablero().get()[0][2] = 4;					           
		juegoAlMoverDerecha.tablero().get()[0][3] = 4;	
		juegoAlMoverDerecha.tablero().get()[2][1] = 2;					     	     
		juegoAlMoverDerecha.tablero().get()[2][2] = 4;					           
		juegoAlMoverDerecha.tablero().get()[2][3] = 4;
				
		juego.mover(39); //simulo movimiento a la derecha
		assertEquals(juegoAlMoverDerecha,juego);
	}
	
	@Test
	public void testIzquierda4Filas4Casos() 
	{
		//matriz:		matrizResultDer:
//		 - 4 2 2			4 4 - -
//		 4 4 2 2			8 4 - -
//		 - 2 2 4			4 4 - -	
//		 4 2 2 2			4 4 2 -
		Juego juego = new Juego(); //Creo la grilla inicial	
		juego.tablero().get()[0][1] = 4;						
		juego.tablero().get()[0][2] = 2;			     		
		juego.tablero().get()[0][3] = 2;	
		juego.tablero().get()[1][0] = 4;						
		juego.tablero().get()[1][1] = 4;						
		juego.tablero().get()[1][2] = 2;			     		
		juego.tablero().get()[1][3] = 2;
		juego.tablero().get()[2][1] = 2;						
		juego.tablero().get()[2][2] = 2;			     		
		juego.tablero().get()[2][3] = 4;
		juego.tablero().get()[3][0] = 4;						
		juego.tablero().get()[3][1] = 2;						
		juego.tablero().get()[3][2] = 2;			     		
		juego.tablero().get()[3][3] = 2; 
					
		Juego juegoAlmoverIzquierda = new Juego(); //Creo la grilla resultante esperada
		juegoAlmoverIzquierda.tablero().get()[0][0] = 4;							
		juegoAlmoverIzquierda.tablero().get()[0][1] = 4;	
		juegoAlmoverIzquierda.tablero().get()[1][0] = 8;							
		juegoAlmoverIzquierda.tablero().get()[1][1] = 4;	
		juegoAlmoverIzquierda.tablero().get()[2][0] = 4;				
		juegoAlmoverIzquierda.tablero().get()[2][1] = 4;	
		juegoAlmoverIzquierda.tablero().get()[3][0] = 4;							
		juegoAlmoverIzquierda.tablero().get()[3][1] = 4;					     	     
		juegoAlmoverIzquierda.tablero().get()[3][2] = 2;	
		
		juego.mover(37); //simulo movimiento a la izquierda
		assertEquals(juegoAlmoverIzquierda,juego);
	}
	
	@Test
	public void testIzquierdaCasoDoble() 
	{
		//matriz:		matrizResultDer:
//	     4 2 2 2			4 4 2 -
//	     - 16 8 8			16 16 - -
//	     2 4 4 4			2 8 4 -
//	     - - - - 			- - - -
		Juego juego = new Juego(); //Creo la grilla inicial
		juego.tablero().get()[0][0] = 4;						
		juego.tablero().get()[0][1] = 2;						
		juego.tablero().get()[0][2] = 2;			     		
		juego.tablero().get()[0][3] = 2;
		juego.tablero().get()[1][1] = 16;						
		juego.tablero().get()[1][2] = 8;			     		
		juego.tablero().get()[1][3] = 8; 
		juego.tablero().get()[2][0] = 2;						
		juego.tablero().get()[2][1] = 4;						
		juego.tablero().get()[2][2] = 4;			     		
		juego.tablero().get()[2][3] = 4; 
		
		Juego juegoAlMoverIzquierda = new Juego(); //Creo la grilla resultante esperada
		juegoAlMoverIzquierda.tablero().get()[0][0] = 4;							
		juegoAlMoverIzquierda.tablero().get()[0][1] = 4;					     	     
		juegoAlMoverIzquierda.tablero().get()[0][2] = 2;	
		juegoAlMoverIzquierda.tablero().get()[1][0] = 16;							
		juegoAlMoverIzquierda.tablero().get()[1][1] = 16;	
		juegoAlMoverIzquierda.tablero().get()[2][0] = 2;							
		juegoAlMoverIzquierda.tablero().get()[2][1] = 8;					     	     
		juegoAlMoverIzquierda.tablero().get()[2][2] = 4;
		
		juego.mover(37); //simulo movimiento a la izquierda
		assertEquals(juegoAlMoverIzquierda,juego);
	}
	
	@Test
	public void testArriba4Columnas4Casos() 
	{
		//matriz:		matrizResult:
//	     - 4 - 4		4 8 4 4
//	     4 4 2 2		4 4 4 4
//	     2 2 2 2		- - - 2
//	     2 2 4 2		- - - -
		Juego juego = new Juego(); //Creo la grilla inicial	
		juego.tablero().get()[1][0] = 4;						
		juego.tablero().get()[2][0] = 2;
		juego.tablero().get()[3][0] = 2;
		juego.tablero().get()[0][1] = 4;					
		juego.tablero().get()[1][1] = 4;					
		juego.tablero().get()[2][1] = 2;
		juego.tablero().get()[3][1] = 2;
		juego.tablero().get()[1][2] = 2;						
		juego.tablero().get()[2][2] = 2;
		juego.tablero().get()[3][2] = 4;
		juego.tablero().get()[0][3] = 4;						
		juego.tablero().get()[1][3] = 2;						
		juego.tablero().get()[2][3] = 2;
		juego.tablero().get()[3][3] = 2;
			
		Juego juegoAlMoverArriba = new Juego(); //Creo la grilla resultante esperada
		juegoAlMoverArriba.tablero().get()[0][0] = 4;						    
		juegoAlMoverArriba.tablero().get()[1][0] = 4;	
		juegoAlMoverArriba.tablero().get()[0][1] = 8;						
		juegoAlMoverArriba.tablero().get()[1][1] = 4;	
		juegoAlMoverArriba.tablero().get()[0][2] = 4;							  
		juegoAlMoverArriba.tablero().get()[1][2] = 4;	
		juegoAlMoverArriba.tablero().get()[0][3] = 4;							
		juegoAlMoverArriba.tablero().get()[1][3] = 4;						       
		juegoAlMoverArriba.tablero().get()[2][3] = 2;
	
		juego.mover(38); //simulo movimiento arriba
		
		assertEquals(juegoAlMoverArriba,juego);
	}
	
	@Test
	public void testAbajo4Columnas4Casos() 
	{
		//matriz:			matrizResult:
//	     2 2 4 2			- - - -
//	     2 2 2 2			- - - 2
//	     4 4 2 2			4 4 4 2
//	     - 4 - 4			4 8 4 4
		Juego juego = new Juego(); //Creo la grilla inicial
		juego.tablero().get()[0][0] = 2;						
		juego.tablero().get()[1][0] = 2;						
		juego.tablero().get()[2][0] = 4;
		juego.tablero().get()[0][1] = 2;		
		juego.tablero().get()[1][1] = 2;						
		juego.tablero().get()[2][1] = 4;
		juego.tablero().get()[3][1] = 4;
		juego.tablero().get()[0][2] = 4;		
		juego.tablero().get()[1][2] = 2;						
		juego.tablero().get()[2][2] = 2;
		juego.tablero().get()[0][3] = 2;						
		juego.tablero().get()[1][3] = 2;						
		juego.tablero().get()[2][3] = 2;
		juego.tablero().get()[3][3] = 4;
		
		Juego juegoAlmoverAbajo = new Juego(); //Creo la grilla resultante esperada
		juegoAlmoverAbajo.tablero().get()[2][0] = 4;
		juegoAlmoverAbajo.tablero().get()[3][0] = 4;
		juegoAlmoverAbajo.tablero().get()[2][1] = 4;
		juegoAlmoverAbajo.tablero().get()[3][1] = 8;
		juegoAlmoverAbajo.tablero().get()[2][2] = 4;
		juegoAlmoverAbajo.tablero().get()[3][2] = 4;
		juegoAlmoverAbajo.tablero().get()[1][3] = 2;						         
		juegoAlmoverAbajo.tablero().get()[2][3] = 4;
		juegoAlmoverAbajo.tablero().get()[3][3] = 4;
	
		juego.mover(40); //simulo movimiento abajo
		assertEquals(juegoAlmoverAbajo,juego);
	}
	
	@Test
	public void tableroLlenoTest() 
	{
		Juego juego = new Juego(); 
		juego.nuevaPartida("Test");
		for(int i=0; i<juego.tablero().largo() ; ++i)
			for(int j=0; j<juego.tablero().largo(); ++j)	
				juego.tablero().get()[i][j] = 2;
		
		assertTrue(juego.tablero().tableroLleno());
	}
	
	@Test
	public void tableroLlenoParcialmenteTest() 
	{
		Juego juego = new Juego(); 
		juego.nuevaPartida("Test");
		for(int i=1; i<juego.tablero().largo() ; ++i)
			for(int j=0; j<juego.tablero().largo(); ++j)	
				juego.tablero().get()[i][j] = 2;
		
		assertFalse(juego.tablero().tableroLleno());
	}
	
	@Test
	public void tableroNMismosNrosTest() 
	{
		Juego juego = new Juego(); 
		juego.nuevaPartida("Test");
		for(int i=0; i<juego.tablero().largo() ; ++i)
			for(int j=0; j<juego.tablero().largo(); ++j)	
				juego.tablero().get()[i][j] = 2;
		
		assertFalse(juego.tableroTerminado());
	}
	
	@Test
	public void tableroTerminadoSinPosibleMovimTest()
	{
		Juego ju = new Juego();
		ju.nuevaPartida("Test");
		ju.tablero().get()[0][0] = 2;						
		ju.tablero().get()[1][0] = 4;						
		ju.tablero().get()[2][0] = 8;
		ju.tablero().get()[3][0] = 4;
		ju.tablero().get()[0][1] = 8;		
		ju.tablero().get()[1][1] = 2;						
		ju.tablero().get()[2][1] = 16;
		ju.tablero().get()[3][1] = 32;
		ju.tablero().get()[0][2] = 2;		
		ju.tablero().get()[1][2] = 64;						
		ju.tablero().get()[2][2] = 4;
		ju.tablero().get()[3][2] = 8;
		ju.tablero().get()[0][3] = 128;						
		ju.tablero().get()[1][3] = 2;						
		ju.tablero().get()[2][3] = 256;
		ju.tablero().get()[3][3] = 4;	
		
		assertTrue(ju.tableroTerminado());
	}
	
	@Test
	public void juegoTerminadoIgual2048() 
	{
		Juego juegoGanador = new Juego();
		juegoGanador.nuevaPartida("Test");
		juegoGanador.tablero().get()[1][1] = 2048;
		juegoGanador.actualizarPuntaje();
		
		assertTrue(juegoGanador.juegoTerminado());
	}
	
	@Test
	public void juegoTerminadoMenor2048() 
	{
		Juego juego = new Juego();
		juego.nuevaPartida("Test");
		juego.tablero().get()[3][3] = 2047;
		juego.actualizarPuntaje();
		
		assertFalse(juego.juegoTerminado());
	}
	
	@Test
	public void actualizarPuntajeGrillaConUnMaximo()
	{
		Juego jue2 = new Juego();
		jue2.nuevaPartida("Test");
		jue2.tablero().get()[3][3] = 500;
		jue2.tablero().get()[3][0] = 50;
		
		jue2.actualizarPuntaje();
		
		assertEquals(500, jue2.getPuntaje());
	}
	
	@Test
	public void insertarNuevoNumeroUnLugarLibre()
	{
		Juego jue = new Juego();
		jue.nuevaPartida("Test");
		jue.tablero().get()[0][0] = 2;						
		jue.tablero().get()[1][0] = 4;						
		jue.tablero().get()[2][0] = 8;
		jue.tablero().get()[3][0] = 4;
		jue.tablero().get()[0][1] = 8;		
		jue.tablero().get()[1][1] = 2;						
		jue.tablero().get()[2][1] = 16;
		jue.tablero().get()[3][1] = 32;
		jue.tablero().get()[0][2] = 2;		
		jue.tablero().get()[1][2] = 64;						
		jue.tablero().get()[2][2] = 4;
		jue.tablero().get()[3][2] = 0;
		jue.tablero().get()[0][3] = 128;						
		jue.tablero().get()[1][3] = 2;						
		jue.tablero().get()[2][3] = 256;
		jue.tablero().get()[3][3] = 4;	
		
		jue.insertarNuevoNumero();
		jue.tablero();
		
		assertTrue(jue.tablero().tableroLleno());
	}
	
	@Test
	public void resetTablero()
	{
		Tablero tableroInicial = new Tablero();
		tableroInicial.get()[0][0] = 1;
		tableroInicial.get()[0][3] = 1;
		tableroInicial.get()[3][0] = 1;
		tableroInicial.get()[3][3] = 1;
		
		Tablero tableroFinal = new Tablero(); //Lo dejo todo en cero
		
		tableroInicial.resetTablero();
		
		assertEquals(tableroFinal, tableroInicial);
	}
}
