package logica;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

public class Juego 
{
	String nombreJuego;
	String jugadorActual;
	Tablero tablero;	
	
	int puntaje = 0;
	String[] mejorMarca;
	int record = 0;
	boolean habilitarJuego = false;
	String mensajeFinDeJuego;
	String estado;
	
	Map<String,Integer> partidas; //guardo los datos de las partidas
	
	public Juego()
	{
		partidas = new HashMap<String,Integer>();
		nombreJuego = "2048";
		jugadorActual = "";
		mensajeFinDeJuego = "";
		estado = "";
		mejorMarca = new String[2];
		tablero = new Tablero();		
	}
	
	//inicio de un juego por jugador
	public boolean nuevaPartida(String usuario)
	{
		cargarHistorico();
		
		if(partidas.containsKey(usuario))
			return false;		
		partidas.put(usuario, puntaje);
		
		tablero.resetTablero(); 
		resetPuntaje();
		resetMensajeFinal();
		insertarNuevoNumero();
		setJugadorActual(usuario);
		habilitarJuego();
		
		return true;
	}
	
	//reinicio juego mismo usuario
	public void volverAEmpezar()
	{		
		tablero.resetTablero(); 
		resetPuntaje();
		insertarNuevoNumero(); 
	}
	
	public String getEstado()
	{
		return this.estado;
	}
	
	public Map<String,Integer> getPartidas()
	{
		return this.partidas;
	}
	
	private void setEstado(String estado)
	{
		this.estado = estado;
	}
	
	private void cerrarPartida()
	{
		if(estado == "ganador")
			mensajeFinDeJuego = "Felicitaciones, " + jugadorActual + "!";
		else if(estado == "perdedor")
			mensajeFinDeJuego = "Perdiste, " + jugadorActual;
		
		asignarPuntajeFinal();
		establecerRecord();
		desHabilitarJuego();
		grabarHistorico();		
	}
	
	private void establecerRecord()
	{
		Iterator<Map.Entry<String, Integer>> it = partidas.entrySet().iterator();
		
		Map.Entry<String, Integer> reg;			
		while (it.hasNext())
		{
			reg = it.next();
			if(partidas.get(reg.getKey()) > record)
			{
				record = partidas.get(reg.getKey());
			}			
		}	
	}
	
	public String getRecord()
	{
		return record != 0 ? record+"" : " - ";
	}
	
	private void asignarPuntajeFinal()
	{
		partidas.put(jugadorActual, puntaje);
	}
	
	public boolean getHabilitado()
	{
		return habilitarJuego;
	}
	
	public String getTxtUsuarioIncorrecto()
	{
		return "El nombre de usuario debe tener\nentre exceder los 8 caracteres";
	}
	
	public String getTxtUsuarioExistente()
	{
		return "Usuario existente. Ingrese uno diferente, por favor.";
	}
		
	private boolean habilitarJuego()
	{		
		return habilitarJuego = true;
	}
	
	private boolean desHabilitarJuego()
	{		
		return habilitarJuego = false;
	}
	
	public String getMensajeFinal()
	{
		return mensajeFinDeJuego;
	}
	
	private void resetMensajeFinal()
	{
		mensajeFinDeJuego = "";
	}
	
	public int[] sugerenciaJugada()
	{
		actualizarPuntaje(); //para asegurarme tener el �ltimo mayor puntaje
		
		int[] jugada = new int[3]; // [0] = pos i, [1] = pos j, [2] sentido
		
		int max = puntaje; //mayor puntaje		
		
		while(max != 1)
		{
			for(int i=0; i < tablero.largo() ; ++i)
			{			
				for(int j=0; j < tablero.largo(); ++j)
					if(tablero.get()[i][j] == max )
					{
						int sentido = tablero.devolverSentidoParaMover(i,j);
						if(sentido != 0) 
						{
							jugada[0] = i;
							jugada[1] = j;
							jugada[2] = sentido;
							return jugada;
						}
					}
												
			}
			max = max /2; //no encuentra jugada con el mayor, voy con el siguiente mayor
		}
		
		return jugada;
	}
	
	public int getPuntaje()
	{
		return puntaje;
	}
	
	public Tablero tablero()
	{					
		return tablero;
	}
	
	private void resetPuntaje()
	{
		puntaje = 0;
	}
	
	private void setPuntaje(int puntos)
	{
		puntaje = puntos;
	}
	
	public String getNombreJuego()
	{
		return nombreJuego;
	}
	
	private void setJugadorActual(String jugador)
	{
		jugadorActual = jugador;
	}
	
	public void actualizarPuntaje()
	{
		int max = 0;
		
		for(int i=0; i < tablero.largo() ; ++i)
		{			
			for(int j=0; j < tablero.largo(); ++j)
				if(tablero.get()[i][j] > max )
					max = tablero.get()[i][j];
		}
		
		setPuntaje(max); 
	}	

	public void insertarNuevoNumero()
	{
		Random r = new Random();
		boolean flag = true;
		
		while(flag) // Hasta que encuentre una posicion con valor 0
		{
			int x = r.nextInt(4);
			int y = r.nextInt(4);
			
			if(tablero.get()[x][y] == 0)
			{
				tablero.get()[x][y] = this.nuevoNumero(); 
				flag = false;
			}
		}
	}
	
	// Damos por "terminado" el tablero cuando est� lleno y 
	// no hay posibilidad de movimiento al insertar el �ltimo nro
	public boolean tableroTerminado()
	{
		int movimiento = this.sugerenciaJugada()[2];
		
		if(tablero.tableroLleno() && movimiento == 0)
			return true;
		
		return false;
	}
	
	private int nuevoNumero()
	{
		Random r = new Random();
		int n = r.nextInt(2);
		
		if(n == 0)
			return 2;
		else
			return 4;
	}
	
	public boolean tableroLleno()
	{
		return tablero.tableroLleno();
	}
	
	public boolean juegoTerminado()
	{
		if (ganoJuego()) 
		{
			setEstado("ganador");
			cerrarPartida();
			return true;
		}			
		else if (tableroTerminado())
		{
			setEstado("perdedor");
			cerrarPartida();
			return true;
		}
		
		return false;
	}
	
	public boolean ganoJuego()
	{
		if(puntaje == 2048)
			return true;
		return false;
	}
	
	public void mover(int tecla)
	{
		switch(tecla)
		{
			case 37: tablero.moverIzquierda();
				break;
			case 38: tablero.moverArriba();
				break;
			case 39: tablero.moverDerecha();
				break;
			case 40: tablero.moverAbajo();
				break;		
		}
		
		actualizarPuntaje(); //luego de mover actualiza el puntaje
	}
	
	private void grabarHistorico()
	{
        try 
        {
        	File dir = new File(".");
        	String ruta = dir.getCanonicalPath();
            String rutaCompleta = ruta + "/historico.txt";
            
            File file = new File(rutaCompleta);
            
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            
            StringBuilder archivo = new StringBuilder();
            
            Iterator<Map.Entry<String, Integer>> it = partidas.entrySet().iterator();
			
			Map.Entry<String, Integer> reg;			
			while (it.hasNext())
			{
				reg = it.next();
				if(!reg.getKey().equals("") && !reg.getKey().equals("Test"))
					archivo.append(reg.getKey() + "-" + partidas.get(reg.getKey()) + "\n");
			}	
            
            
            bw.write(archivo.toString());
            bw.close();
        } 
        catch(Exception e) 
        {
            e.printStackTrace();
        }
    }
	
	private void cargarHistorico()
	{
        try 
        {
        	File dir = new File(".");
        	String ruta = dir.getCanonicalPath();
            String rutaCompleta = ruta + "/historico.txt";
            
            File file = new File(rutaCompleta);
            String texto = "";
            if (file.exists()) 
            {
            	FileReader lector=new FileReader(file);
            	BufferedReader contenido=new BufferedReader(lector);
            	while((texto=contenido.readLine())!=null)
            	{
            		if(!texto.equals("") && !texto.equals("Test"))
            		{
            			String temp = texto;
                		String[] split = temp.split("-");
                		partidas.put(split[0], Integer.parseInt(split[1]));
            		}            		
            	}     
            	
            	contenido.close();
            	establecerRecord();
            }
        } 
        catch(Exception e) 
        {
            e.printStackTrace();
        }
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tablero == null) ? 0 : tablero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Juego other = (Juego) obj;
		if (tablero == null) {
			if (other.tablero != null)
				return false;
		} else if (!tablero.equals(other.tablero))
			return false;
		return true;
	}
	
}

