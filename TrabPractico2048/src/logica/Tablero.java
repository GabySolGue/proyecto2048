package logica;

import java.util.Arrays;

public class Tablero {
	
	int [][] tablero;
	boolean [][] controlSuma; //para verificar que cada celda no haya duplicado en un movimiento
	
	public Tablero()
	{
		tablero = new int [4][4];
		
		for(int i=0; i < this.largo() ; ++i)
			for(int j=0; j < this.largo(); ++j)
				this.get()[i][j] = 0; 
		
		controlSuma = new boolean[4][4]; //creo una "grilla" paralela con valores en false
		
		for(int i=0; i < controlSuma.length ; ++i)
			for(int j=0; j < controlSuma.length; ++j) 
				controlSuma[i][j] = false;
	}
	
	public int largo()
	{
		return tablero.length;
	}
	
	public int[][] get()
	{
		return tablero;
	}
	
	// Verifico si hay al menos una posicion en null. Peor caso: i = j = 3
	public boolean tableroLleno()
	{
		for(int i=0; i<this.largo() ; ++i)
			for(int j=0; j<this.largo(); ++j)
				if(tablero[i][j] == 0)
					return false;
		return true;
	}
	
	// reinicio valores del tablero en 0
	public void resetTablero()
	{
		for(int i=0; i < this.largo() ; ++i)
			for(int j=0; j < this.largo(); ++j)
				this.get()[i][j] = 0; 
	}
	
	//reinicio todo el tablero de control en false
	private void resetControl()
	{
		for(int i=0; i < controlSuma.length ; ++i)
			for(int j=0; j < controlSuma.length; ++j)
				controlSuma[i][j] = false;
	}
	
	//me fijo de no pasarme de los limites del tablero
	private boolean excedeLimite(int a)
	{
		return a < 0 || a >= this.largo() ;
	}
	
	// Me muevo arriba = resto en el indice i
	public void moverArriba()
	{
		for(int j = 0; j < this.largo(); ++j)
			for(int i = 1; i < this.largo(); ++i) // Desde la fila 2 a la 4
			{
				int increm = -1;
				
				//mientras se mueva a casillero en 0 
				while(!excedeLimite(i+increm) && tablero[i+increm][j] == 0)				
				{
					tablero[i+increm][j] = tablero[i+increm+1][j];
					tablero[i+increm+1][j] = 0;
					increm--;
				}					
				
				//luego de toparse con un valor != 0 evalua si suma (o resta):
				sumoEnVertical(i,j,increm);
			}
		resetControl();
	}	
	
	// Me muevo a la derecha = sumo en el indice j	
	public void moverDerecha()
	{			
		for(int i = 0; i < this.largo(); ++i)
			for(int j = this.largo() - 2; j >= 0 ; --j) // Desde columna 3 a la 1
			{
				int increm = 1;
				while(!excedeLimite(j+increm) && tablero[i][j+increm] == 0)				
				{
					tablero[i][j+increm] = tablero[i][j+increm-1];
					tablero[i][j+increm-1] = 0;
					increm++;
				}					
				
				sumoEnHorizontal(i,j,increm);
			}
		resetControl();
	}
	
	// Me muevo abajo = sumo en el indice i
	public void moverAbajo()
	{	
		for(int j = 0; j < this.largo(); ++j)
			for(int i = this.largo() - 2; i >= 0 ; --i) // Desde fila 3 a la 1
			{
				int increm = 1;
				while(!excedeLimite(i+increm) && tablero[i+increm][j] == 0)				
				{
					tablero[i+increm][j] = tablero[i+increm-1][j];
					tablero[i+increm-1][j] = 0;
					increm++;
				}					
				
				sumoEnVertical(i,j,increm);
			}	
		resetControl();
	}
	
	// Me muevo a la izquierda = resto en el indice j
	public void moverIzquierda()
	{		
		for(int i = 0; i < this.largo(); ++i)
			for(int j = 1; j < this.largo() ; ++j) // Desde columna 2 a la 4	
			{
				int increm = -1;
				while(!excedeLimite(j+increm) && tablero[i][j+increm] == 0)				
				{
					tablero[i][j+increm] = tablero[i][j+increm+1];
					tablero[i][j+increm+1] = 0;
					increm--;
				}					
				
				sumoEnHorizontal(i,j,increm);
			}
		resetControl();
	}
	
	//esto vale para los movimientos DERECHA e IZQUIERDA
	private void sumoEnHorizontal(int fila, int col, int increm)
	{
		if(!excedeLimite(col+increm) && !controlSuma[fila][col+increm])
		{
			if(increm > 0 && tablero[fila][col+increm] == tablero[fila][col+increm-1])
			{
				controlSuma[fila][col+increm] = true;
				tablero[fila][col+increm] = tablero[fila][col+increm-1] * 2;
				tablero[fila][col+increm-1] = 0;
			}
			else if(increm < 0 && tablero[fila][col+increm] == tablero[fila][col+increm+1])
			{
				controlSuma[fila][col+increm] = true;
				tablero[fila][col+increm] = tablero[fila][col+increm+1] * 2;
				tablero[fila][col+increm+1] = 0;
			}
		}
	}
	
	//esto vale para los movimientos ARRIBA y ABAJO
	private void sumoEnVertical(int fila, int col, int increm)
	{
		if(!excedeLimite(fila+increm) && !controlSuma[fila+increm][col])
		{
			if(increm > 0 && tablero[fila+increm][col] == tablero[fila+increm-1][col])
			{
				controlSuma[fila+increm][col] = true;
				tablero[fila+increm][col] = tablero[fila+increm-1][col] * 2;
				tablero[fila+increm-1][col] = 0;
			}
			else if(increm < 0 && tablero[fila+increm][col] == tablero[fila+increm+1][col])
			{
				controlSuma[fila+increm][col] = true;
				tablero[fila+increm][col] = tablero[fila+increm+1][col] * 2;
				tablero[fila+increm+1][col] = 0;
			}
		}
	}
	
	public int devolverSentidoParaMover(int a, int b)
	{
		int increm = 1;
		
		// eval�o hacia arriba dentro del limite de la matriz
		while(!excedeLimite(a-increm))
		{
			if(tablero[a][b] == tablero[a-increm][b]) //es igual retorno arriba
				return 38;
			else if(tablero[a-increm][b] == 0) //es cero, incremento y vuelvo a iterar
				increm++;
			else // es un valor distinto salgo y paso a ver en otro sentido
				break;
		}
			
		increm = 1; //reinicio incremento
		
		// eval�o hacia derecha
		while(!excedeLimite(b+increm))
		{
			if(tablero[a][b] == tablero[a][b+increm])
				return 39;
			else if(tablero[a][b+increm] == 0)
				increm++;
			else
				break;
		}			
		
		increm = 1;
		
		// eval�o hacia abajo
		while(!excedeLimite(a+increm))
		{
			if(tablero[a][b] == tablero[a+increm][b])
				return 40;
			else if(tablero[a+increm][b] == 0)
				increm++;
			else
				break;
		}
		
		
		increm = 1;
		
		// eval�o hacia derecha
		while(!excedeLimite(b-increm))
		{
			if(tablero[a][b] == tablero[a][b-increm])
				return 37;
			else if(tablero[a][b-increm] == 0)
				increm++;
			else
				break;
		}
		
		return 0;
	}
	
	@Override
	public String toString()
	{
		String s = "";
		
		for(int i=0; i<this.largo(); ++i)
		{
			s += " - ";
			for(int j=0; j<this.largo() ; ++j)
				s += tablero[i][j] + " ";
		}			
		
		return s;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(tablero);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tablero other = (Tablero) obj;
		if (!Arrays.deepEquals(tablero, other.tablero))
			return false;
		return true;
	}

}
