package interfaz;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.awt.Color;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import logica.Juego;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextPane;
import javax.swing.JLayeredPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.MatteBorder;
import java.awt.Component;

public class JuegoInterfaz 
{

	private JFrame frame;
	private JTextField txtUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					JuegoInterfaz window = new JuegoInterfaz();
					window.frame.setVisible(true);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JuegoInterfaz() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(120, 200, 200));
		frame.setBounds(100, 100, 490, 510);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setFocusable(true);
		
		// Area nombre del juego
		JLabel titulo = new JLabel();
		titulo.setFont(new Font("OCR A Extended", Font.ITALIC, 70));
		titulo.setBounds(15, 11, 309, 75);
		frame.getContentPane().add(titulo);
		
		JLabel tituloEfecto = new JLabel();
		tituloEfecto.setOpaque(false);
		tituloEfecto.setFont(new Font("OCR A Extended", Font.ITALIC, 70));
		tituloEfecto.setBounds(17, 13, 309, 75);
		tituloEfecto.setForeground(new Color(255, 255, 255));
		frame.getContentPane().add(tituloEfecto);
		
		JLabel tituloEfecto2 = new JLabel();
		tituloEfecto2.setOpaque(false);
		tituloEfecto2.setFont(new Font("OCR A Extended", Font.ITALIC, 70));
		tituloEfecto2.setBounds(21, 15, 309, 75);
		tituloEfecto2.setForeground(new Color(180, 76, 67));
		frame.getContentPane().add(tituloEfecto2);
		
		JLabel lblRecord = new JLabel("Record");
		lblRecord.setOpaque(false);
		lblRecord.setForeground(new Color(19, 90, 166));
		lblRecord.setHorizontalAlignment(SwingConstants.CENTER);
		lblRecord.setFont(new Font("OCR A Extended", Font.BOLD, 15));
		lblRecord.setBounds(330, 20, 135, 20);
		frame.getContentPane().add(lblRecord);
		lblRecord.setVisible(false);
		
		JLabel txtRecord = new JLabel();
		txtRecord.setOpaque(false);
		txtRecord.setForeground(new Color(19, 90, 166));
		txtRecord.setFont(new Font("OCR A Extended", Font.BOLD, 20));
		txtRecord.setHorizontalAlignment(SwingConstants.CENTER);
		txtRecord.setBounds(330, 20, 135, 75);
		frame.getContentPane().add(txtRecord);	
		txtRecord.setVisible(false);
		
		// Vista inicial de bienvenida
		JLayeredPane lyInicio = new JLayeredPane();
		lyInicio.setAlignmentY(Component.TOP_ALIGNMENT);
		lyInicio.setOpaque(true);
		lyInicio.setBackground(new Color(120, 200, 200));
		lyInicio.setBounds(0, 146, 490, 309);
		frame.getContentPane().add(lyInicio);
		lyInicio.setLayout(null);
		
		JLabel bienvenido = new JLabel();
		bienvenido.setText("Inicia una partida");
		bienvenido.setFont(new Font("OCR A Extended", Font.BOLD, 25));
		bienvenido.setBounds(95, 10, 300, 75);
		lyInicio.add(bienvenido);
		
		JLabel bienvenidoEfecto1 = new JLabel();
		bienvenidoEfecto1.setText("Inicia una partida");
		bienvenidoEfecto1.setFont(new Font("OCR A Extended", Font.BOLD, 25));
		bienvenidoEfecto1.setBounds(96, 11, 300, 75);
		bienvenidoEfecto1.setForeground(new Color(255, 255, 255));
		lyInicio.add(bienvenidoEfecto1);
		
		JLabel bienvenidoEfecto2 = new JLabel();
		bienvenidoEfecto2.setText("Inicia una partida");
		bienvenidoEfecto2.setFont(new Font("OCR A Extended", Font.BOLD, 25));
		bienvenidoEfecto2.setBounds(97, 12, 300, 75);
		bienvenidoEfecto2.setForeground(new Color(180, 76, 67));
		lyInicio.add(bienvenidoEfecto2);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(120, 103, 66, 36);
		lblUsuario.setFont(new Font("OCR A Extended", Font.BOLD, 14));
		lyInicio.add(lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(200, 107, 90, 28);
		lyInicio.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JButton btnEmpezar = new JButton("Empezar!");
		btnEmpezar.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnEmpezar.setBounds(200, 168, 90, 23);
		lyInicio.add(btnEmpezar);
		
		JLabel lblErrorUsuario = new JLabel();
		lblErrorUsuario.setBounds(95, 222, 300, 45);
		lblErrorUsuario.setForeground(new Color(165, 42, 42));
		lblErrorUsuario.setHorizontalAlignment(JLabel.CENTER);
		lblErrorUsuario.setBackground(new Color(120, 200, 200));
		lyInicio.add(lblErrorUsuario);
		
		// Vista de fin del juego
		JLabel txtFinDelJuego = new JLabel();
		txtFinDelJuego.setAlignmentX(Component.CENTER_ALIGNMENT);
		txtFinDelJuego.setBorder(null);
		txtFinDelJuego.setAlignmentY(Component.TOP_ALIGNMENT);
		txtFinDelJuego.setOpaque(true);
		txtFinDelJuego.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 32));
		txtFinDelJuego.setBackground(new Color(120, 200, 200));
		txtFinDelJuego.setBounds(0, 109, 490, 346);
		txtFinDelJuego.setHorizontalAlignment(JLabel.CENTER);
		frame.getContentPane().add(txtFinDelJuego);
		txtFinDelJuego.setVisible(false	);
		
		JLabel txtFinal= new JLabel();
		txtFinal.setHorizontalAlignment(JLabel.CENTER);
		txtFinal.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 32));
		txtFinal.setBounds(0, 80, 490, 30);
		txtFinDelJuego.add(txtFinal);
		
		JLabel txtGanaste = new JLabel("GANASTE");
		txtGanaste.setFont(new Font("Franklin Gothic Medium", Font.PLAIN, 32));
		txtGanaste.setHorizontalAlignment(JLabel.CENTER);
		txtGanaste.setBounds(0, 120, 490, 30);
		txtGanaste.setForeground(new Color(49,127,67));
		
		JLabel lblPuntFinal = new JLabel();
		lblPuntFinal.setOpaque(false);
		lblPuntFinal.setForeground(new Color(220, 20, 60));
		lblPuntFinal.setFont(new Font("Franklin Gothic Medium", Font.BOLD, 25));
		lblPuntFinal.setHorizontalAlignment(SwingConstants.CENTER);
		lblPuntFinal.setBounds(0, 200, 490, 30);
		txtFinDelJuego.add(lblPuntFinal);
				
		JButton btnOKFin = new JButton("OK");
		btnOKFin.setBounds(215, 260, 50, 50);
		btnOKFin.setFont(new Font("Franklin Gothic Medium", Font.BOLD, 12));
		btnOKFin.setHorizontalAlignment(SwingConstants.CENTER);
		txtFinDelJuego.add(btnOKFin);
		
		// Zona juego		
		JLabel lblPuntaje = new JLabel("Puntaje");
		lblPuntaje.setOpaque(false);
		lblPuntaje.setForeground(new Color(255, 255, 255));
		lblPuntaje.setHorizontalAlignment(SwingConstants.CENTER);
		lblPuntaje.setFont(new Font("OCR A Extended", Font.BOLD, 23));
		lblPuntaje.setBounds(330, 120, 135, 26);
		frame.getContentPane().add(lblPuntaje);
		
		JLabel txtPuntaje = new JLabel();
		txtPuntaje.setOpaque(false);
		txtPuntaje.setForeground(new Color(255, 255, 255));
		txtPuntaje.setFont(new Font("Verdana", Font.BOLD, 35));
		txtPuntaje.setHorizontalAlignment(SwingConstants.CENTER);
		txtPuntaje.setBounds(329, 146, 135, 75);
		frame.getContentPane().add(txtPuntaje);	
		
		JLabel label0_0 = new JLabel();
		label0_0.setFont(new Font("Verdana", Font.BOLD, 25));
		label0_0.setHorizontalAlignment(SwingConstants.CENTER);
		label0_0.setOpaque(true);
		label0_0.setBackground(Color.WHITE);
		label0_0.setBounds(10, 146, 75, 75);
		frame.getContentPane().add(label0_0);
		
		JLabel label0_1 = new JLabel();
		label0_1.setFont(new Font("Verdana", Font.BOLD, 25));
		label0_1.setHorizontalAlignment(SwingConstants.CENTER);
		label0_1.setOpaque(true);
		label0_1.setBackground(Color.WHITE);
		label0_1.setBounds(88, 146, 75, 75);
		frame.getContentPane().add(label0_1);
		
		JLabel label0_2 = new JLabel();
		label0_2.setFont(new Font("Verdana", Font.BOLD, 25));
		label0_2.setHorizontalAlignment(SwingConstants.CENTER);
		label0_2.setOpaque(true);
		label0_2.setBackground(Color.WHITE);
		label0_2.setBounds(166, 146, 75, 75);
		frame.getContentPane().add(label0_2);
		
		JLabel label0_3 = new JLabel();
		label0_3.setFont(new Font("Verdana", Font.BOLD, 25));
		label0_3.setHorizontalAlignment(SwingConstants.CENTER);
		label0_3.setOpaque(true);
		label0_3.setBackground(Color.WHITE);
		label0_3.setBounds(244, 146, 75, 75);
		frame.getContentPane().add(label0_3);		
		
		JLabel label1_0 = new JLabel();
		label1_0.setFont(new Font("Verdana", Font.BOLD, 25));
		label1_0.setHorizontalAlignment(SwingConstants.CENTER);
		label1_0.setOpaque(true);
		label1_0.setBackground(Color.WHITE);
		label1_0.setBounds(10, 224, 75, 75);
		frame.getContentPane().add(label1_0);
		
		JLabel label1_1 = new JLabel();
		label1_1.setFont(new Font("Verdana", Font.BOLD, 25));
		label1_1.setHorizontalAlignment(SwingConstants.CENTER);
		label1_1.setOpaque(true);
		label1_1.setBackground(Color.WHITE);
		label1_1.setBounds(88, 224, 75, 75);
		frame.getContentPane().add(label1_1);
		
		JLabel label1_2 = new JLabel();
		label1_2.setFont(new Font("Verdana", Font.BOLD, 25));
		label1_2.setHorizontalAlignment(SwingConstants.CENTER);
		label1_2.setOpaque(true);
		label1_2.setBackground(Color.WHITE);
		label1_2.setBounds(166, 224, 75, 75);
		frame.getContentPane().add(label1_2);
		
		JLabel label1_3 = new JLabel();
		label1_3.setFont(new Font("Verdana", Font.BOLD, 25));
		label1_3.setHorizontalAlignment(SwingConstants.CENTER);
		label1_3.setOpaque(true);
		label1_3.setBackground(Color.WHITE);
		label1_3.setBounds(244, 224, 75, 75);
		frame.getContentPane().add(label1_3);
		
		JLabel label2_0 = new JLabel();
		label2_0.setFont(new Font("Verdana", Font.BOLD, 25));
		label2_0.setHorizontalAlignment(SwingConstants.CENTER);
		label2_0.setOpaque(true);
		label2_0.setBackground(Color.WHITE);
		label2_0.setBounds(10, 302, 75, 75);
		frame.getContentPane().add(label2_0);
		
		JLabel label2_1 = new JLabel();
		label2_1.setFont(new Font("Verdana", Font.BOLD, 25));
		label2_1.setHorizontalAlignment(SwingConstants.CENTER);
		label2_1.setOpaque(true);
		label2_1.setBackground(Color.WHITE);
		label2_1.setBounds(88, 302, 75, 75);
		frame.getContentPane().add(label2_1);
		
		JLabel label2_2 = new JLabel();
		label2_2.setFont(new Font("Verdana", Font.BOLD, 25));
		label2_2.setHorizontalAlignment(SwingConstants.CENTER);
		label2_2.setOpaque(true);
		label2_2.setBackground(Color.WHITE);
		label2_2.setBounds(166, 302, 75, 75);
		frame.getContentPane().add(label2_2);
		
		JLabel label2_3 = new JLabel();
		label2_3.setFont(new Font("Verdana", Font.BOLD, 25));
		label2_3.setHorizontalAlignment(SwingConstants.CENTER);
		label2_3.setOpaque(true);
		label2_3.setBackground(Color.WHITE);
		label2_3.setBounds(244, 302, 75, 75);
		frame.getContentPane().add(label2_3);	
		
		JLabel label3_0 = new JLabel();
		label3_0.setFont(new Font("Verdana", Font.BOLD, 25));
		label3_0.setHorizontalAlignment(SwingConstants.CENTER);
		label3_0.setOpaque(true);
		label3_0.setBackground(Color.WHITE);
		label3_0.setBounds(10, 380, 75, 75);
		frame.getContentPane().add(label3_0);
		
		JLabel label3_1 = new JLabel();
		label3_1.setFont(new Font("Verdana", Font.BOLD, 25));
		label3_1.setHorizontalAlignment(SwingConstants.CENTER);
		label3_1.setOpaque(true);
		label3_1.setBackground(Color.WHITE);
		label3_1.setBounds(88, 380, 75, 75);
		frame.getContentPane().add(label3_1);
		
		JLabel label3_2 = new JLabel();
		label3_2.setFont(new Font("Verdana", Font.BOLD, 25));
		label3_2.setHorizontalAlignment(SwingConstants.CENTER);
		label3_2.setOpaque(true);
		label3_2.setBackground(Color.WHITE);
		label3_2.setBounds(166, 380, 75, 75);
		frame.getContentPane().add(label3_2);
		
		JLabel label3_3 = new JLabel();
		label3_3.setFont(new Font("Verdana", Font.BOLD, 25));
		label3_3.setHorizontalAlignment(SwingConstants.CENTER);
		label3_3.setOpaque(true);
		label3_3.setBackground(Color.WHITE);
		label3_3.setBounds(244, 380, 75, 75);
		frame.getContentPane().add(label3_3);
		
		JLabel[][] labels = new JLabel[4][4];
		labels[0][0] = label0_0;
		labels[0][1] = label0_1;
		labels[0][2] = label0_2;
		labels[0][3] = label0_3;
		labels[1][0] = label1_0;
		labels[1][1] = label1_1;
		labels[1][2] = label1_2;
		labels[1][3] = label1_3;
		labels[2][0] = label2_0;
		labels[2][1] = label2_1;
		labels[2][2] = label2_2;
		labels[2][3] = label2_3;
		labels[3][0] = label3_0;
		labels[3][1] = label3_1;
		labels[3][2] = label3_2;
		labels[3][3] = label3_3;
		
		JButton btnReinicio = new JButton("Reiniciar");
		btnReinicio.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnReinicio.setForeground(new Color(0, 0, 0));
		btnReinicio.setFont(new Font("Verdana", Font.BOLD, 11));
		btnReinicio.setBackground(new Color(220, 220, 220));
		btnReinicio.setBounds(10, 109, 75, 26);
		frame.getContentPane().add(btnReinicio);
		
		JButton btnSugerencia = new JButton("Mostrar jugada");
		btnSugerencia.setToolTipText("Sugiere una jugada posible");
		btnSugerencia.setBorder(new SoftBevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnSugerencia.setBackground(new Color(230, 230, 250));
		btnSugerencia.setFont(new Font("Verdana", Font.BOLD, 11));
		btnSugerencia.setBounds(332, 300, 125, 26);
		frame.getContentPane().add(btnSugerencia);
		lblPuntaje.setVisible(false);
		txtPuntaje.setVisible(false);
		btnReinicio.setVisible(false);
		
		JLabel lblSugerenciaArriba = new JLabel("");
		lblSugerenciaArriba.setBounds(329, 337, 135, 69);
		frame.getContentPane().add(lblSugerenciaArriba);
		
		JLabel lblSugerenciaDerecha = new JLabel("");
		lblSugerenciaDerecha.setBounds(329, 337, 135, 75);
		frame.getContentPane().add(lblSugerenciaDerecha);
		
		JLabel lblSugerenciaAbajo = new JLabel("");
		lblSugerenciaAbajo.setBounds(329, 337, 135, 75);
		frame.getContentPane().add(lblSugerenciaAbajo);
		
		JLabel lblSugerenciaIzquierda = new JLabel("");
		lblSugerenciaIzquierda.setBounds(329, 337, 135, 75);
		frame.getContentPane().add(lblSugerenciaIzquierda);
		
		JTextPane txtNoHaySugerencia = new JTextPane();
		txtNoHaySugerencia.setOpaque(false);
		txtNoHaySugerencia.setFont(new Font("Verdana", Font.PLAIN, 14));
		txtNoHaySugerencia.setBounds(348, 337, 135, 75);
		frame.getContentPane().add(txtNoHaySugerencia);
		txtNoHaySugerencia.setVisible(false);
		
		lblSugerenciaAbajo.setVisible(false);
		lblSugerenciaIzquierda.setVisible(false);	
		lblSugerenciaArriba.setVisible(false);
		lblSugerenciaDerecha.setVisible(false);
		btnSugerencia.setVisible(false);
		btnOKFin.setVisible(false);		
		
		File dir = new File(".");
		try 
		{
			String ruta = dir.getCanonicalPath();
			ImageIcon imgArriba = new ImageIcon(ruta + "\\imagenes\\arriba.png");
			Icon iconoArriba = new ImageIcon(imgArriba.getImage().getScaledInstance(lblSugerenciaArriba.getWidth(), 
					lblSugerenciaArriba.getHeight(), Image.SCALE_DEFAULT));
			lblSugerenciaArriba.setIcon(iconoArriba);
			
			ImageIcon imgDerecha = new ImageIcon(ruta + "\\imagenes\\derecha.png");
			Icon iconoDerecha = new ImageIcon(imgDerecha.getImage().getScaledInstance(lblSugerenciaDerecha.getWidth(), 
					lblSugerenciaDerecha.getHeight(), Image.SCALE_DEFAULT));
			lblSugerenciaDerecha.setIcon(iconoDerecha);
			
			ImageIcon imgAbajo = new ImageIcon(ruta + "\\imagenes\\abajo.png");
			Icon iconoAbajo = new ImageIcon(imgAbajo.getImage().getScaledInstance(lblSugerenciaAbajo.getWidth(), 
					lblSugerenciaAbajo.getHeight(), Image.SCALE_DEFAULT));
			lblSugerenciaAbajo.setIcon(iconoAbajo);
			
			ImageIcon imgIzquierda = new ImageIcon(ruta + "\\imagenes\\izquierda.png");
			Icon iconoIzquierda = new ImageIcon(imgIzquierda.getImage().getScaledInstance(lblSugerenciaIzquierda.getWidth(), 
					lblSugerenciaIzquierda.getHeight(), Image.SCALE_DEFAULT));
			lblSugerenciaIzquierda.setIcon(iconoIzquierda);
		} 
		catch (IOException e1) 
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
		
		Juego nuevoJuego = new Juego();		
		
		titulo.setText(nuevoJuego.getNombreJuego());
		tituloEfecto.setText(nuevoJuego.getNombreJuego());
		tituloEfecto2.setText(nuevoJuego.getNombreJuego());
		
		btnEmpezar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{					
				String nombreUsuario = txtUsuario.getText();
				
				if(nombreUsuario.length() == 0)
					lblErrorUsuario.setText("Debe ingresar un nombre de usuario");
				else if(nombreUsuario.length() > 8)
					lblErrorUsuario.setText("El nombre de usuario debe tener hasta 8 caracteres");			
				else 
				{
					if(nuevoJuego.nuevaPartida(nombreUsuario))
					{
						txtRecord.setText(nuevoJuego.getRecord());
						txtRecord.setVisible(true);
						lblRecord.setVisible(true);
						
						lyInicio.setVisible(false);
						lblUsuario.setVisible(false);
						txtUsuario.setVisible(false);
						btnEmpezar.setVisible(false);
						lblErrorUsuario.setText("");
						txtUsuario.setText("");
						
						mostrarTablero(labels);
						lblPuntaje.setVisible(true);
						txtPuntaje.setVisible(true);
						btnReinicio.setVisible(true);
						
						txtPuntaje.setText(nuevoJuego.getPuntaje()+"");					
						dibujarNumeros(nuevoJuego.tablero().get(), labels);
					}
					else
						lblErrorUsuario.setText(nuevoJuego.getTxtUsuarioExistente());
				}				
			}
		});
		
		btnReinicio.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				nuevoJuego.volverAEmpezar();
				txtPuntaje.setText(nuevoJuego.getPuntaje()+"");
				dibujarNumeros(nuevoJuego.tablero().get(), labels);				
				frame.requestFocus();
			}
		});
		
		btnOKFin.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				lyInicio.setVisible(true);
				lblUsuario.setVisible(true);
				txtUsuario.setVisible(true);
				btnEmpezar.setVisible(true);
				
				ocultarTablero(labels);
				lblPuntaje.setVisible(false);
				txtPuntaje.setVisible(false);
				btnSugerencia.setVisible(false);
				btnReinicio.setVisible(false);
				txtFinDelJuego.setVisible(false);
				txtRecord.setVisible(false);
				lblRecord.setVisible(false);
			}
		});
		
		frame.addKeyListener(new KeyListener() 
		{ 
			@Override 
			public void keyTyped(KeyEvent e) { }
			
			@Override 
			public void keyReleased(KeyEvent e) { }
			
            @Override 
            public void keyPressed(KeyEvent e)
            { 
            	if(nuevoJuego.getHabilitado())
            	{
            		if(!nuevoJuego.juegoTerminado())
	            	{
	            		//oculto todas las fechas del movimiento sugerido
	            		btnSugerencia.setVisible(true);
	            		lblSugerenciaArriba.setVisible(false);
	            		lblSugerenciaDerecha.setVisible(false);
	            		lblSugerenciaAbajo.setVisible(false);
	            		lblSugerenciaIzquierda.setVisible(false);
	            		txtNoHaySugerencia.setVisible(false);
	            		
	            		int tecla = e.getKeyCode();
	            		
	            		if (tecla == KeyEvent.VK_UP || tecla == KeyEvent.VK_DOWN || tecla == KeyEvent.VK_LEFT || tecla == KeyEvent.VK_RIGHT) 
	            		{
	            			nuevoJuego.mover(tecla); 
	    					dibujarNumeros(nuevoJuego.tablero().get(), labels); 
	    					nuevoNumero();
	            		}
	            		
	            	}
	            	if(nuevoJuego.juegoTerminado())
	            	{					
						ocultarTablero(labels);
						lblPuntaje.setVisible(false);
						txtPuntaje.setVisible(false);
						btnSugerencia.setVisible(false);
						btnReinicio.setVisible(false);
						txtRecord.setText(nuevoJuego.getRecord());
	            		txtFinDelJuego.setVisible(true);
	            		btnOKFin.setVisible(true);
	            		
	            		txtFinal.setText(nuevoJuego.getMensajeFinal());
	            		lblPuntFinal.setText("Tu puntaje: " + nuevoJuego.getPuntaje()+"");
	            		
	            		if(nuevoJuego.getEstado().equals("ganador"))
	            		{
	            			txtFinDelJuego.add(txtGanaste);
	            			txtFinal.setForeground(new Color(49,127,67));
	            		}
	            		else if(nuevoJuego.getEstado().equals("perdedor"))
	            			txtFinal.setForeground(new Color(121,85,61));
	            	} 
            	}	            	
            } 
            
            private void nuevoNumero()
        	{
        		if(!nuevoJuego.tableroLleno())
					nuevoJuego.insertarNuevoNumero();
        		txtPuntaje.setText(nuevoJuego.getPuntaje()+"");
				dibujarNumeros(nuevoJuego.tablero().get(), labels); 
        	}
        });
		
		//Cuando presiono el boton en pantalla busca la jugada sugerida
		btnSugerencia.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0)
			{
				int[] datos = nuevoJuego.sugerenciaJugada(); //trae pos i, pos j, y sentido
				if(datos[2] != 0)
				{
					labels[datos[0]][datos[1]].setBorder(new MatteBorder(5, 5, 5, 5, (Color) new Color(255, 255, 0)));
					switch(datos[2])
					{
					// hago visible la fecha del sentido sugerido
						case 38: lblSugerenciaArriba.setVisible(true);				
							break;
						case 39: lblSugerenciaDerecha.setVisible(true);				
							break;
						case 40: lblSugerenciaAbajo.setVisible(true);				
							break;
						case 37: lblSugerenciaIzquierda.setVisible(true);
							break;
					}
				}
				else
				{
					txtNoHaySugerencia.setVisible(true);
					txtNoHaySugerencia.setText("No se detecta jugada posible");
				}
				
				frame.requestFocus();
			}	
		});

	}
	
	private void ocultarTablero(JLabel[][] labels)
	{
		for(int i=0; i<labels.length ; ++i)
			for(int j=0; j<labels.length; ++j) 			
				labels[i][j].setVisible(false); 
					
	}
	
	private void mostrarTablero(JLabel[][] labels)
	{
		for(int i=0; i<labels.length ; ++i)
			for(int j=0; j<labels.length; ++j) 			
				labels[i][j].setVisible(true); 
					
	}
	
	private void resetearCasillero(JLabel label)
	{		
		label.setText(""); // quito el texto del numero que tenga
		label.setBackground(new Color(255,255,255)); //dejo con fondo blanco (para el caso de los 0s)
		label.setBorder(null);
	}
	
	private void dibujarNumeros(int[][] grilla, JLabel[][] labels)
	{
		for(int i=0; i<grilla.length ; ++i)
			for(int j=0; j<grilla.length; ++j) 
			{
				resetearCasillero(labels[i][j]);
				
				if(grilla[i][j] != 0) 
				{
					labels[i][j].setText(grilla[i][j]+""); // vuelvo a escribir si no es cero
					colorear(labels[i][j]);	//voy asignando nuevo color al casillero
				}				
			}
	}
	
	private void colorear(JLabel label)
	{
		int numero =  Integer.parseInt(label.getText());
		
		//En funcion del numero que contenga asigno valores rgb:
		if(numero <= 4)
			label.setBackground(new Color(255 - (numero * 10), 255 - (numero * 10), 255));
		else if(numero > 4 && numero <= 16)
			label.setBackground(new Color(255 - (numero * 4), 255 - (numero * 4), 255 - (numero * 4)));
		else if(numero > 16 && numero < 128)
			label.setBackground(new Color(255 - numero/2, 255 - (numero * 2), 255));
		else if(numero >= 128 && numero <= 512)
			label.setBackground(new Color(255 - numero/8, 255, 255 - numero/4));
		else if(numero == 2048)
		{
			label.setBackground(new Color(95, 115, 255));
		}
			
	}
}
